<div<?php print $attributes; ?>>
    <div class="l-unconstrained--user">
        <user class="l-user" role="aside">

            <div class="l-user-cart">
                <?php print render($page['cart']); ?>
            </div>
        </user>
    </div>
    <div class="l-unconstrained-header">
        <header class="l-header" role="banner">

            <div class="l-branding">
                <?php if ($logo): ?>
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"
                       class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/></a>
                <?php endif; ?>

                <?php if ($site_name || $site_slogan): ?>
                    <?php if ($site_name): ?>
                        <h1 class="site-name">
                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"
                               rel="home"><span><?php print $site_name; ?></span></a>
                        </h1>
                    <?php endif; ?>

                    <?php if ($site_slogan): ?>
                        <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
                    <?php endif; ?>
                <?php endif; ?>

                <?php print render($page['branding']); ?>
            </div>

            <?php print render($page['header']); ?>

        </header>
    </div>
    <?php if ($page['navigation']): ?>
        <div class="l-unconstrained-navigation">
            <div class="l-navigation">
                <navigation>
                    <?php print render($page['navigation']); ?>
                </navigation>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($page['slideshow']): ?>
      <div class="l-unconstrained-slideshow">
        <div class="l-slider">
            <aside>
                <?php print render($page['slideshow']); ?>
            </aside>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($page['content_first']): ?>
        <div class="l-unconstrained-content-first">
            <div class="l-content-first"><?php print render($page['content_first']); ?></div>
        </div>
    <?php endif; ?>




    <div class="l-unconstrained-main">
        <?php if ($show_breadcrumb): ?>
            <div class="l-breadcrumb">
                <?php print $breadcrumb; ?>
            </div>
        <?php endif; ?>
        <div class="l-main">

            <div class="l-content" role="main">
                <?php print render($page['highlighted']); ?>
                <?php //print $breadcrumb; ?>
                <a id="main-content"></a>
                <?php print render($title_prefix); ?>
                <?php if ($show_title && $title): ?>
                    <h1><?php print $title; ?></h1>
                <?php endif; ?>
                <?php print render($title_suffix); ?>
                <?php print $messages; ?>
                <?php print render($tabs); ?>
                <?php print render($page['help']); ?>
                <?php if ($action_links): ?>
                    <ul class="action-links"><?php print render($action_links); ?></ul>
                <?php endif; ?>

                <?php print render($page['content']); ?>

                <?php print $feed_icons; ?>


            </div>

            <?php print render($page['sidebar_first']); ?>
            <?php print render($page['sidebar_second']); ?>


        </div>



        <?php if (($page['blogrelated'])): ?>
            <div class="l-blogrelated" role="aside">
                <?php print render($page['blogrelated']); ?>
            </div>
        <?php endif; ?>


        <footer class="l-footer" role="contentinfo">
            <div class="footer-1"><?php print render($page['footer_1']); ?></div>
            <div class="footer-2"><?php print render($page['footer_2']); ?></div>
            <div class="footer-3"><?php print render($page['footer_3']); ?></div>
            <div class="footer-4"><?php print render($page['footer_4']); ?></div>
            <div class="footer-main"><?php print render($page['footer']); ?></div>
        </footer>


    </div>
</div>
